# Running Metabase on Kubernetes
This guide will help you install Metabase on Kubernetes using [Metabase Helm chart](https://github.com/kubernetes/charts/tree/master/stable/metabase)

## Prerequisites
- Kubernetes 1.4+ with Beta APIs enabled
- [Kubernetes Helm](https://github.com/kubernetes/helm) installed

## Installing
To install with the release name `my-release`:
```
 $ helm install my-release stable/metabase
```
To genetare the metabase-config.yaml with the default values:
```
 $ helm get values my-release -a -output yaml > metabase-config.yaml
```
## Configuring
By default, backend database (H2) is stored inside container, and will be lost after container restart.
So we highly recommended to use MySQL or Postgres instead.
Copy these default configuration into [metabase-config.yaml](https://gitlab.com/_cedon/metabse/-/blob/master/metabase-config.yaml), then modify as your need.

Deploy Metabase using your config file:

```
$ helm install --name my-release -f metabase-config.yaml stable/metabase
```

## Generate Template
To run with kuberneetes sdk defaults(without helm):
set your configurantion on [metabase-config.yaml](https://gitlab.com/_cedon/metabse/-/blob/master/metabase-config.yaml)
generate the template on directory ./manifests:
```
$ helm template --values ./metabase-config.yaml --output-dir ./manifests stable/metabase
```

see the files with your configuration on path `./manifests/metabase`

